<?php
$servername = "localhost";
$username = "root";
$password = "";


function execQuery($conn , $sql , $message){
    if ($conn->query($sql) === TRUE) {
        echo $message;
    } else {
        echo "Error" . $message." " .$conn->error;
    }
    echo "<br>";
}

// Create connection
$conn = mysqli_connect($servername, $username, $password);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Create database
// $sql = "CREATE DATABASE myTable";
// execQuery($conn , $sql , "Create");

// $sql = "USE myTable";
// execQuery($conn , $sql , "USE");

$sql = "CREATE TABLE MyClient (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Nume VARCHAR(30) NOT NULL,
    Email VARCHAR(50),
    Numar_De_Telefon VARCHAR(20),
    Mesaj VARCHAR(200),
   
    )";
    execQuery($conn, $sql, "Table");

mysqli_close($conn);
?>