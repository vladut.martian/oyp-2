<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link href="/oyp-2/oyp-2/boost/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/oyp=2/oyp-2/boost/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet'
    type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="">
  <link href="/oyp-2/oyp-2/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link href="/oyp-2/oyp-2/css/sb-admin-2.min.css" rel="stylesheet">

  <title>Document</title>
</head>

<body>



<?php
// $servername = "localhost";
// $username = "root";
// $password = "";
// $dbname = "myTable";

// // Create connection
// $conn = new mysqli($servername, $username, $password, $dbname);
// // Check connection
// if ($conn->connect_error) {
//     die("Connection failed: " . $conn->connect_error);
// }

$mess='';

if(isset($_POST['submit'])) {
  $name = $_POST['name'];
  $email = $_POST['email'];
  $phoneNr = $_POST['phone'];
  $message = $_POST['message'];


$sql = "INSERT INTO MyClient (Nume, Email, Numar_De_Telefon, Mesaj)
VALUES ('$name', '$email', '$phoneNr' , '$message')";
if($sql){

  $mess="Studentul ".$name.' a fost adaugat cu succes!';

}else {
  $mess='A aparut o eroare!';
}

}
// if ($conn->query($sql) === TRUE) {
//     echo "New record created successfully";
// } else {
//     echo "Error: " . $sql . "<br>" . $conn->error;
// }

?>
  

  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.php">SC Contrust SRL</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
        data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
        aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
     </nav>


  <header class="masthead">
    <div class="container">
      <div class="intro-text">
        <div class="intro-lead-in">Bine ai venit!</div>
        <div class="intro-heading text-uppercase">Inovatia incepe cu tine</div>

      </div>
    </div>
  </header>


  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Clientii nostri</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Nume</th>
              <th>Email</th>
              <th>Numar de telefon</th>
              <th>Mesaj</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Nume</th>
              <th>Email</th>
              <th>Numar de telefon</th>
              <th>Mesaj</th>
            </tr>
          </tfoot>
          <tbody>
            

          <?php

function execQuery($conn , $sql , $message){
    if ($conn->query($sql) === TRUE) {
        echo $message;
    } else {
        echo "Error" . $message." " .$conn->error;
    }
    echo "<br>";
}
$servername = "localhost";
$username = "root";
$password = "";
$dbName = "myTable";


// Create connection
$conn = new mysqli($servername, $username, $password, $dbName);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$sql = "SELECT  * FROM MyClient";
$result = $conn->query($sql);
// execQuery($conn, $sql, "Select");

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      ?>
      <tr>
      <td><?php echo $row['Nume'] ?></td>
      <td><?php echo $row['Email'] ?></td>
      <td><?php echo $row['Numar_De_Telefon'] ?></td>
      <td><?php echo $row['Mesaj'] ?></td>
      
   
    </tr>
    <?php 
    }
} else {
    echo "0 results";
}

$conn->close();
?>

          </tbody>
        </table>
      </div>
    </div>
  </div>

  </div>


  </div>


  <script src="/oyp-2/oyp-2/boost/jquery/jquery.min.js"></script>
  <script src="/oyp-2/oyp-2/boost/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="js/sb-admin-2.min.js"></script>

  <script src="/oyp-2/oyp-2/boost/jquery-easing/jquery.easing.min.js"></script>
  <script src="bundle.js"></script>

  <script src="/oyp-2/oyp-2/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="/oyp-2/oyp-2/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <script src="/oyp-2/oyp-2/js/demo/datatables-demo.js"></script>
</body>

</html>