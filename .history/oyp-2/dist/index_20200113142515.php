<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SC Contrust SRL</title>

  <!-- Bootstrap core CSS -->
  <link href="/oyp-2/oyp-2/boost/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/oyp-2/oyp-2/boost/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet'
    type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>



  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>





</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">SC Contrust SRL</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
        data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
        aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#services">Servicii</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#portfolio">Proiecte</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about">Despre noi</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#team">Ce facem</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="table.php">Clientii</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Header -->
  <header class="masthead">
    <div class="container">
      <div class="intro-text">
        <div class="intro-lead-in">Bine ai venit!</div>
        <div class="intro-heading text-uppercase">Inovatia incepe cu tine</div>

      </div>
    </div>
  </header>

  <!-- Services -->
  <section class="page-section" id="services">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Servicii</h2>
          <h3 class="section-subheading text-muted">Cand vine vorba de construit ne place sa credem ca suntem cei mai
            buni .</h3>
        </div>
      </div>
      <div class="row text-center">
        <div class="col-md-4">
          <span class="fa-stack fa-4x">

          </span>
          <h4 class="service-heading">Proiectare</h4>
          <p class="text-muted">Oferim suport clientilor nostri pentru conceptia a ceva nou si nemaintalnit</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">

          </span>
          <h4 class="service-heading">Executare</h4>
          <p class="text-muted">Nu suntem doar cei care gandim o idee suntem si cei care ne pricepem sa o executam
            gratie unei experiente de aproape 25 ani.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">

          </span>
          <h4 class="service-heading">Renovare</h4>
          <p class="text-muted">Aducem tot ceea ce este vechi si demodat la nivel acutal</p>
        </div>
      </div>
    </div>
  </section>



  <!-- Projects -->
  <section class="bg-light page-section" id="portfolio">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Proiectele noastre</h2>
          <h3 class="section-subheading text-muted">Încercăm să fim cei mai buni înceea ce facem</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-6 portfolio-item">

          <div class="portfolio-hover">
            <div class="portfolio-hover-content">

            </div>
          </div>
          <img class="img-fluid" src="/oyp-2/oyp-2/src/img/rebat.jpg" alt="">
          </a>
          <div class="portfolio-caption">
            <h4>Rebat Copsa-Mica</h4>
            <p class="text-muted">un punct de lucru SC Rombat Sa</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">

          <div class="portfolio-hover">
            <div class="portfolio-hover-content">

            </div>
          </div>
          <img class="img-fluid" src="/oyp-2/oyp-2/src/img/rombat.jpg" alt="">
          </a>
          <div class="portfolio-caption">
            <h4>SC Rombat SA</h4>
            <p class="text-muted">Punct de lucru in Bucuresti</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">

          <div class="portfolio-hover">
            <div class="portfolio-hover-content">

            </div>
          </div>
          <img class="img-fluid"
            src="/oyp-2/oyp-2/src/img/intrarea-in-muzeul-podgoriei-muzeul-viei-si-vinului-din-dragasani-judetul-valcea.jpg"
            alt="">
          </a>
          <div class="portfolio-caption">
            <h4>Muzeul viei si al Vinului</h4>
            <p class="text-muted">Monument istoric renovat </p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">

          <div class="portfolio-hover">
            <div class="portfolio-hover-content">

            </div>
          </div>
          <img class="img-fluid" src="/oyp-2/oyp-2/src/img/irina.jpg" alt="">
          </a>
          <div class="portfolio-caption">
            <h4>Irina Impex SRL</h4>
            <p class="text-muted">Hala metalica</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">

          <div class="portfolio-hover">
            <div class="portfolio-hover-content">

            </div>
          </div>
          <img class="img-fluid" src="/oyp-2/oyp-2/src/img/hotel-president-32.jpg" alt="">
          </a>
          <div class="portfolio-caption">
            <h4>Hotel President - Baile Olanesti</h4>
            <p class="text-muted">Cladire renovata pentru utilizarea in scop turistic</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">

          <div class="portfolio-hover">
            <div class="portfolio-hover-content">

            </div>
          </div>
          <img class="img-fluid" src="/oyp-2/oyp-2/src/img/rar2.jpg" alt="">
          </a>
          <div class="portfolio-caption">
            <h4>Reprezentanta Registrul Auto Roman</h4>
            <p class="text-muted">Reprezentanta din Ramnicu-Valcea</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- About -->
  <section class="page-section" id="about">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Despre noi</h2>
          <h3 class="section-subheading text-muted">Istoria începe în 1994 și continuă până în ziua de astăzi.</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <ul class="timeline">
            <li>
              <div class="timeline-image">
                <img class="rounded-circle img-fluid"
                  src="/oyp-2/oyp-2/src/img/about/construction-clipart-general-contractor-1.png" alt="">
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h4>Totul incepe de aici</h4>
                  <h4 class="subheading">De aici a inceput totul. Este locul in care compania noastra s-a dezvoltat
                    incet , dar sigur spre un viitor promitator</h4>
                </div>
                <div class="timeline-body">
                  <p class="text-muted"></p>
                </div>
              </div>
            </li>
            <li class="timeline-inverted">
              <div class="timeline-image">
                <img class="rounded-circle img-fluid" src="/oyp-2/oyp-2/src/img/about/-logo.jpg" alt="">
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">

                  <h4 class="subheading">Un nou competitor a aparut la orizont</h4>
                </div>
                <div class="timeline-body">
                  <p class="text-muted">Numele si logo-ul nostru a inceput incet, incet sa fie recunoscut din ce in ce
                    mai des </p>
                </div>
              </div>
            </li>
            <li>
              <div class="timeline-image">
                <img class="rounded-circle img-fluid" src="/oyp-2/oyp-2/src/img/about/descărcare (1).jpg" alt="">
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h4>Dezvoltare continua </h4>
                  <h4 class="subheading">Ne staduim sa facem totul cel mai bine</h4>
                </div>
                <div class="timeline-body">
                  <p class="text-muted">Odata cu trecea timpului ne-am dezvoltat pe toate planurile si incercam sa
                    continuam acest proces, pentru a oferi cele mai bune rezultate</p>
                </div>
              </div>
            </li>
            <li class="timeline-inverted">
              <div class="timeline-image">
                <img class="rounded-circle img-fluid"
                  src="/oyp-2/oyp-2/src/img/about/Thompson-Construction-Engineering-Precast_Gallery-6_South-Canterbury.jpg"
                  alt="">
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h4>Performanta</h4>
                  <h4 class="subheading">A fi cel mai bun reprezinta un tel al nostru</h4>
                </div>

              </div>
            </li>
            <li class="timeline-inverted">
              <div class="timeline-image">
                <h4>Fii parte
                  <br>din povestea
                  <br>Noastra!</h4>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>


  <section class="bg-light page-section" id="team">
    <div class="container">
      <h2>In ce ne specializam</h2>
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
          <li data-target="#myCarousel" data-slide-to="3"></li>
          <li data-target="#myCarousel" data-slide-to="4"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">

          <div class="item active">
            <img src="/oyp-2/oyp-2/src/img/servicii/proiectare-constructii.jpg" alt="Los Angeles" style="width:100%;">
            <div class="carousel-caption">
              <h3>Punem toate lucrurile la punct</h3>
              <p></p>
            </div>
          </div>

          <div class="item">
            <img src="/oyp-2/oyp-2/src/img/servicii/lucrari-urbanistice-drumuri.jpg" alt="Chicago" style="width:100%;">
            <div class="carousel-caption">
              <h3>Lucrari urbanistice</h3>
              <p>Ne adaptam si mergem mai departe</p>
            </div>
          </div>

          <div class="item">
            <img src="/oyp-2/oyp-2/src/img/servicii/about_img.jpg" alt="New York" style="width:100%;">
            <div class="carousel-caption">
              <h3>Invatand ajingi departe</h3>
              <p>Noi nu ne rezumam doar la ceea ce stim deja ci vrem sa invatam din ce in ce mai mult</p>
            </div>
          </div>

          <div class="item">
            <img src="/oyp-2/oyp-2/src/img/servicii/dirigentie-santier.jpg" alt="New York" style="width:100%;">
            <div class="carousel-caption">
              <h3>Calitatea este prioritatea nostra</h3>
              <p>Avem cei mai buni profesioneti in domniul constructilor</p>
            </div>
          </div>

          <div class="item">
            <img src="/oyp-2/oyp-2/src/img/servicii/executare-tamplarie-pvc.jpg" alt="New York" style="width:100%;">
            <div class="carousel-caption">
              <h3></h3>
              <p></p>
            </div>
          </div>



        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </section>



  <!-- Clients -->
  <section class="py-5">
    <h1>Clientii nostri</h1>
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <a href="#">
            <img class="img-fluid d-block mx-auto" src="/oyp-2/oyp-2/src/img/clientii/registru_auto_roman.jpg" alt="">
          </a>
        </div>
        <div class="col-md-3 col-sm-6">
          <a href="#">
            <img class="img-fluid d-block mx-auto" src="/oyp-2/oyp-2/src/img/clientii/images.png" alt="">
          </a>
        </div>
        <div class="col-md-3 col-sm-6">
          <a href="#">
            <img class="img-fluid d-block mx-auto" src="/oyp-2/oyp-2/src/img/clientii/DSC_1313.JPG" alt="">
          </a>
        </div>
        <div class="col-md-3 col-sm-6">
          <a href="#">
            <img class="img-fluid d-block mx-auto" src="/oyp-2/oyp-2/src/img/clientii/2197hotel_prezident1hotel.jpg" alt="">
          </a>
        </div>
      </div>
    </div>
  </section>

  <!-- Contact -->
  <section class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Contacteaza-ne </h2>
          <h3 class="section-subheading text-muted">Daca esti interesat de o colaborare sau vreun proiect important.
          </h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form id="contactForm" name="sentMessage" novalidate="novalidate">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input class="form-control" id="name" name="name" type="text" placeholder="Nume & Prenume *" required="required"
                    data-validation-required-message=" Introduceti numele complet.">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="email" name="email" type="text" placeholder="Email-ul tau *" required="required"
                    data-validation-required-message="Introduceti adresa de emanil.">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="phone" name="phone" type="text" placeholder="Numarul de telefon *"
                    required="required" data-validation-required-message="Introduceti numarul dumneavoastra de telefon">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <textarea class="form-control" id="message" name=message placeholder="Mesaj *" required="required"
                    data-validation-required-message="Intriduceti mesajul dumneavoastra."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center">
                <div id="success"></div>
                <!-- <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase"
                  type="submit" name="submit">Trimite</button> -->
                  <input type="submit" name="submit" value="Submit">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>


  <?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "myTable";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$mess='';

if(isset($_POST['submit'])) {
  $name = $_POST['name'];
  $email = $_POST['email'];
  $phoneNr = $_POST['phone'];
  $message = $_POST['message'];


$sql = "INSERT INTO MyClient (Nume, Email, Numar_De_Telefon, Mesaj)
VALUES ('$name', '$email', '$phoneNr' , '$message')";
if($sql){

  $mess="Studentul ".$nume.' a fost adaugat cu succes!';

}else {
  $mess='A aparut o eroare!';
}

}

// if ($conn->query($sql) === TRUE) {
//   echo "New record created successfully";
// } else {
//   echo "Error: " . $sql . "<br>" . $conn->error;
// }

$conn->close();
?>

<!-- Footer -->
  <footer class="footer">
    <div class="container">
      <div class="row align-items-center">

        <div class="col-md-3">
          <ul class="list-inline social-buttons">

            <li class="list-inline-item">
              <a href="https://www.facebook.com/contrustvl">
                <i class="fab fa-facebook-f"></i>
              </a>

            </li>

          </ul>
        </div>
        <div class="col-md-3">
          <p>Poti sa ne gasesti si pe Facebook</p>
        </div>

      </div>
    </div>
  </footer>




  <script src="/oyp-2/oyp-2/boost/jquery/jquery.min.js"></script>
  <script src="/oyp-2/oyp-2/boost/bootstrap/js/bootstrap.bundle.min.js"></script>


  <script src="/oyp-2/oyp-2/boost/jquery-easing/jquery.easing.min.js"></script>
  <script src="bundle.js"></script>


</body>

</html>