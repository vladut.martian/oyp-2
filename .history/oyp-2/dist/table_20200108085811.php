<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="/oyp-2/boost/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/oyp-2/boost/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet'
        type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <title>Document</title>
</head>

<body>
  <?php
require("src/contact/function.php");
queryResult("DELETE FROM customer WHERE id=".$_GET['id']);

  ?>

    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top">SC Contrust SRL</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ml-auto">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="index.html">Acasa</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="table.html">Clientii</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <header class="masthead">
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Bine ai venit!</div>
                <div class="intro-heading text-uppercase">Inovatia incepe cu tine</div>

            </div>
        </div>
    </header>


    <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nume</th>
                  <th>Email</th>
                  <th>Numar de telefon</th>
                  <th>Mesaj</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                <th>Nume</th>
                  <th>Email</th>
                  <th>Numar de telefon</th>
                  <th>Mesaj</th>
                </tr>
              </tfoot>
              <tbody>

              <?php

$result = queryResult("SELECT * FROM MyClient");

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      ?>
      <tr>
      <td><?php echo $row['Nume'] ?></td>
      <td><?php echo $row['Email'] ?></td>
      <td><?php echo $row['Numar_De_Telefon'] ?></td>
      <td><?php echo $row['Mesaj'] ?></td>
      <td><a href=" ">Edit</a> / <a href="tables.php?action=delete&id=<?php echo $row['ID']?>">Delete</a></td>
      
   
    </tr> 

    <?php
    }
  }
 else {
    echo "0 results";
}

?>
               
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
   

  </div>


    <script src="/oyp-2/boost/jquery/jquery.min.js"></script>
    <script src="/oyp-2/boost/bootstrap/js/bootstrap.bundle.min.js"></script>


    <script src="/oyp-2/boost/jquery-easing/jquery.easing.min.js"></script>
    <script src="bundle.js"></script>

</body>

</html>